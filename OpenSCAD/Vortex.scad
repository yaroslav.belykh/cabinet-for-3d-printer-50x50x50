difference(){
union(){
    difference(){
translate([0,0,1]) cube( size=[136,128,2], center=true );
union(){
translate([-52.3,-52.3,-2]) cylinder(h=6, r1=2.3, r2=2.3, center=false, $fn=500);
translate([-52.3, 52.3,-2]) cylinder(h=6, r1=2.3, r2=2.3, center=false, $fn=500);
translate([ 52.3,-52.3,-2]) cylinder(h=6, r1=2.3, r2=2.3, center=false, $fn=500);
translate([ 52.3, 52.3,-2]) cylinder(h=6, r1=2.3, r2=2.3, center=false, $fn=500);
}}
translate([0,0,17]) cylinder(h=60, r1=62, r2=42, center=false, $fn=500);
translate([0,0,2]) cylinder(h=15, r1=62, r2=62, center=false, $fn=500);
difference(){
translate([64,0,9]) cube( size=[8,128,15], center=true );
    union() {
    translate([64,-2,7]) cube( size=[4,134,22], center=true );
        translate([ 62,-58,10]) rotate([0,90,0])  cylinder(h=14, r1=1.5, r2=1.5, center=true, $fn=500);
        translate([ 62, 58,10]) rotate([0,90,0])  cylinder(h=14, r1=1.5, r2=1.5, center=true, $fn=500);
}}
difference(){
translate([-64,0,9]) cube( size=[8,128,15], center=true );
    union() {
    translate([-64,-2,7]) cube( size=[4,134,22], center=true );
        translate([-62,-58,10]) rotate([0,90,0])  cylinder(h=14, r1=1.5, r2=1.5, center=true, $fn=500);
        translate([-62, 58,10]) rotate([0,90,0])  cylinder(h=14, r1=1.5, r2=1.5, center=true, $fn=500);
}}
}
union(){
translate([0,0,16.9]) cylinder(h=60.2, r1=60, r2=40, center=false, $fn=500);
translate([0,0,-1]) cylinder(h=17.905, r1=60, r2=60, center=false, $fn=500);}
}