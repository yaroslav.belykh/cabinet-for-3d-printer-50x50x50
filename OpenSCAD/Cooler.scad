diametreMin = 45 ;
diametreMaxA = 90; 
diametreMaxB = 80; 
diametreMinB = 45 ; 
HeightColler = 45;
placement = 2;
think = 2;
translate([0,0,placement])
    {
    difference()
    {
        union()
        {
            cube ( size=[200,100,2], center=true );
            translate([-diametreMaxA/2-4,0,0]) cylinder(h=10, r1=diametreMaxA / 2 + think, r2=diametreMaxA / 2 + think, center=false, $fn=500);
            translate([-diametreMaxA/2-4,0,10]) cylinder(h=HeightColler, r1=diametreMaxA / 2 + think, r2=diametreMin / 2 + think, center=false, $fn=500);
            translate([diametreMaxB/2+6,0,0]) cylinder(h=10, r1=diametreMaxB / 2 + think, r2=diametreMaxB / 2 + think, center=false, $fn=500);
            translate([diametreMaxB/2+6,0,10]) cylinder(h=HeightColler, r1=diametreMaxB / 2 + think, r2=diametreMin / 2 + think, center=false, $fn=500);
        }
        union()
        {
            translate([-diametreMaxA/2-4,0,-2]) cylinder(h=11, r1=diametreMaxA / 2, r2=diametreMaxA / 2, center=false, $fn=500);
            translate([-diametreMaxA/2-4,0,8]) cylinder(h=HeightColler+3, r1=diametreMaxA / 2, r2=diametreMin / 2, center=false, $fn=500);
            translate([diametreMaxB/2+6,0,-2]) cylinder(h=11, r1=diametreMaxB / 2, r2=diametreMaxB / 2, center=false, $fn=500);
            translate([diametreMaxB/2+6,0,8]) cylinder(h=HeightColler+3, r1=diametreMaxB / 2, r2=diametreMin / 2, center=false, $fn=500);
        }
    }
}
