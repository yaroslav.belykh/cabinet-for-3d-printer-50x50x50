diametreMin = 45 ;
diametreMaxA = 90; 
diametreMaxB = 80;  
HeightColler = 45;
placement = 2;
think = 2;
Borders=1000;

difference()
{
    union()
    {
        cube ( size=[90,90,think], center=true );
        cylinder(h=10, r1=diametreMaxB / 2 + think, r2=diametreMaxB / 2 + think, center=false, $fn=Borders);
        translate([0,0,10]) cylinder(h=HeightColler, r1=diametreMaxB / 2 + think, r2=diametreMin / 2 + think, center=false, $fn=Borders);
        translate([diametreMin/2+0.5,-5,51]) cube ( size=[2,10,42], center=false );
        translate([-diametreMin/2-2.5,-5,51]) cube ( size=[2,10,42], center=false ); 
        
        translate([-45/2-0.5,0,90]) polyhedron(
        points=[ [0,-5,0],[2,-5,0],[2,5,0],[0,5,0],[0,-5,3],[0,5,3]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        translate([45/2+0.5,0,90]) polyhedron(
        points=[ [-2,-5,0],[0,-5,0],[0,5,0],[-2,5,0],[0,-5,3],[0,5,3]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        
        
        translate([-45/2-0.5,0,90]) polyhedron(
        points=[ [0,-5,-1.8],[2,-5,-1.8],[2,5,-1.8],[0,5,-1.8],[0,-5,-3.8],[0,5,-3.8]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        translate([45/2+0.5,0,90]) polyhedron(
        points=[ [-2,-5,-1.8],[0,-5,-1.8],[0,5,-1.8],[-2,5,-1.8],[0,-5,-3.8],[0,5,-3.8]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        
    }
    union()
    {
        translate([0,0,-2]) cylinder(h=13, r1=diametreMaxB / 2, r2=diametreMaxB / 2, center=false, $fn=Borders/2+Borders/4);
        translate([0,0,10]) cylinder(h=HeightColler+think, r1=diametreMaxB / 2, r2=diametreMin / 2, center=false, $fn=Borders/2+Borders/4);
        translate([37.5,37.5,-2]) cylinder(h=10, r1=2.3, r2=2.3,center=true, $fn=Borders/4+Borders/2+Borders/8);
        translate([37.5,-37.5,-2]) cylinder(h=10, r1=2.3, r2=2.3,center=true, $fn=Borders/4+Borders/2+Borders/8);
        translate([-37.5,37.5,-2]) cylinder(h=10, r1=2.3, r2=2.3,center=true, $fn=Borders/4+Borders/2+Borders/8);
        translate([-37.5,-37.5,-2]) cylinder(h=10, r1=2.3, r2=2.3,center=true, $fn=Borders/4+Borders/2+Borders/8);
    }
}
