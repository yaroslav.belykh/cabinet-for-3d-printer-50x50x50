difference(){
union(){
    difference(){
translate([0,0,1]) cube( size=[88,88,2], center=true );
union(){
translate([-35.7,-35.7,-2]) cylinder(h=6, r1=2.3, r2=2.3, center=false, $fn=500);
translate([-35.7, 35.7,-2]) cylinder(h=6, r1=2.3, r2=2.3, center=false, $fn=500);
translate([ 35.7,-35.7,-2]) cylinder(h=6, r1=2.3, r2=2.3, center=false, $fn=500);
translate([ 35.7, 35.7,-2]) cylinder(h=6, r1=2.3, r2=2.3, center=false, $fn=500);
}}
translate([0,0,12]) cylinder(h=45, r1=42, r2=22, center=false, $fn=400);
translate([0,0,2]) cylinder(h=10, r1=42, r2=42, center=false, $fn=400);
}
union(){
translate([0,0,11]) cylinder(h=49, r1=20, r2=20, center=false, $fn=400);
translate([0,0,-1]) cylinder(h=13, r1=40, r2=40, center=false, $fn=400);}
}