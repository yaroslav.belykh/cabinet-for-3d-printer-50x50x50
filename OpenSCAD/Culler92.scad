diametreMin = 45 ;
diametreMaxA = 90; 
diametreMaxB = 80;  
HeightColler = 45;
placement = 2;
think = 2;
Borders=1000;

difference()
{
    union()
    {
        cube ( size=[100,100,think], center=true );
        cylinder(h=10, r1=diametreMaxA / 2 + think, r2=diametreMaxA / 2 + think, center=false, $fn=Borders);
        translate([0,0,10]) cylinder(h=HeightColler, r1=diametreMaxA / 2 + think, r2=diametreMin / 2 + think, center=false, $fn=Borders);
        translate([diametreMin/2+1,-5,51]) cube ( size=[2,10,42], center=false );
        translate([-diametreMin/2-4+1,-5,51]) cube ( size=[2,10,42], center=false ); 
        translate([-2.5,35,21]) cube (size=[5,2,72],center=false);
        translate([-2.5,-35,21]) cube (size=[5,2,72],center=false);
        
        translate([-45/2-1,0,90]) polyhedron(
        points=[ [0,-5,0],[2,-5,0],[2,5,0],[0,5,0],[0,-5,3],[0,5,3]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        translate([45/2+1,0,90]) polyhedron(
        points=[ [-2,-5,0],[0,-5,0],[0,5,0],[-2,5,0],[0,-5,3],[0,5,3]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        
        translate([0,-33,90]) polyhedron(
        points=[ [-2.5,0,0],[-2.5,2,0],[2.5,2,0],[2.5,0,0],[-2.5,0,3],[2.5,0,3]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        translate([0,35,90]) polyhedron(
        points=[ [-2.5,-2,0],[-2.5,0,0],[2.5,0,0],[2.5,-2,0],[-2.5,0,3],[2.5,0,3]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        
        translate([-45/2-1,0,90]) polyhedron(
        points=[ [0,-5,-1.8],[2,-5,-1.8],[2,5,-1.8],[0,5,-1.8],[0,-5,-3.8],[0,5,-3.8]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        translate([45/2+1,0,90]) polyhedron(
        points=[ [-2,-5,-1.8],[0,-5,-1.8],[0,5,-1.8],[-2,5,-1.8],[0,-5,-3.8],[0,5,-3.8]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        
        translate([0,-33,90]) polyhedron(
        points=[ [-2.5,0,-1.8],[-2.5,2,-1.8],[2.5,2,-1.8],[2.5,0,-1.8],[-2.5,0,-3.8],[2.5,0,-3.8]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
        translate([0,35,90]) polyhedron(
        points=[ [-2.5,-2,-1.8],[-2.5,0,-1.8],[2.5,0,-1.8],[2.5,-2,-1.8],[-2.5,0,-3.8],[2.5,0,-3.8]],
        faces=[ [0,1,4],[1,2,5,4],[2,3,5],[0,4,5,3],[0,3,2,1]]
        );
    }
    union()
    {
        translate([0,0,-2]) cylinder(h=13, r1=diametreMaxA / 2, r2=diametreMaxA / 2, center=false, $fn=Borders/2+Borders/4);
        translate([0,0,10]) cylinder(h=HeightColler+think, r1=diametreMaxA / 2, r2=diametreMin / 2, center=false, $fn=Borders/2+Borders/4);
        translate([41.4,41.4,-2]) cylinder(h=10, r1=2.3, r2=2.3,center=true, $fn=Borders/4+Borders/2+Borders/8);
        translate([41.4,-41.4,-2]) cylinder(h=10, r1=2.3, r2=2.3,center=true, $fn=Borders/4+Borders/2+Borders/8);
        translate([-41.4,41.4,-2]) cylinder(h=10, r1=2.3, r2=2.3,center=true, $fn=Borders/4+Borders/2+Borders/8);
        translate([-41.4,-41.4,-2]) cylinder(h=10, r1=2.3, r2=2.3,center=true, $fn=Borders/4+Borders/2+Borders/8);
    }
}
